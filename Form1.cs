
/*
 * Elaborado por Roberth Dudiver Venezuela 
 * roberth_dum@hotmail.com
 * esto lo  hice hace 20 a�os XD 
 * 
 * */

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace Fractal
{
	/// <summary>
	/// Descripci�n breve de Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.ComponentModel.IContainer components;

		public Form1()
		{
			//
			// Necesario para admitir el Dise�ador de Windows Forms
			//
			InitializeComponent();
			Bounds=Screen.AllScreens[Screen.AllScreens.GetLowerBound(0)].Bounds;  

			//
			// TODO: agregar c�digo de constructor despu�s de llamar a InitializeComponent
			//
		}

		/// <summary>
		/// Limpiar los recursos que se est�n utilizando.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region C�digo generado por el Dise�ador de Windows Forms
		/// <summary>
		/// M�todo necesario para admitir el Dise�ador. No se puede modificar
		/// el contenido del m�todo con el editor de c�digo.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(67, 92);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(202, 120);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(0, 283);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(512, 27);
            this.label1.TabIndex = 1;
            this.label1.Text = "Roberth Dudiver  rdudiver@gmail.com";
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(512, 310);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// Punto de entrada principal de la aplicaci�n.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}
		public  class Cursor_ 
		{
			public PointF PUNTO
			{
				get { return punto; }
				set { punto = value; }
			}

			public float ANGULO
			{
				get { return angulo; }
				set { angulo = value; }
			}

			public PointF punto = new PointF(500,500);
			public    float angulo;
		}
		float Punto_Inicial=300, Longitud=45, N�mero_Iteraciones=89;
		float angulo = -5;

		Cursor_ cursor = new Cursor_();
		
		Random h = new Random();
		bool fy;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Label label1;
		Graphics gt;
		public PointF avanzar(float angulo, float d, Color fk)
		{


			PointF p = new PointF(float.Parse(Convert.ToString(cursor.PUNTO.X + d * Math.Cos(cursor.angulo))), float.Parse( Convert.ToString(cursor.PUNTO.Y + d * Math.Sin(-cursor.angulo))));
			// p.X = Convert.ToDouble(cursor.PUNTO.X + d * Math.Cos(cursor.angulo));
			//p.Y = cursor.PUNTO.Y + d *Math.Sin(cursor.angulo);

            

			SolidBrush fr = new SolidBrush(fk);

			if (gh == 0)
			{
				gt.FillRectangle(fr, cursor.PUNTO.X, cursor.PUNTO.Y, 45, 45);
                
			}




			if (gh == 1)
			{
				gt.FillEllipse(fr, cursor.PUNTO.X, cursor.PUNTO.Y, 10, 10);
				gt.DrawEllipse(new Pen(Color.Black, 1), cursor.PUNTO.X, cursor.PUNTO.Y, 10, 10);
       
			}
         
			if(gh==2)
			{
				gt.DrawLine(new Pen(fk, 2), cursor.PUNTO.X, cursor.PUNTO.Y, p.X, p.Y);
			}
			Font fr1 = new Font(this.Font.FontFamily, 25);

			if (gh == 3)
			{
				//gt.DrawString("roberth", fr1, Brushes.White, cursor.PUNTO.X, cursor.PUNTO.Y);
				Random f4 = new Random();
				gt.DrawString(f4.Next(2).ToString() + f4.Next(2).ToString() + f4.Next(1).ToString() + f4.Next(1).ToString(), fr1, fr, cursor.PUNTO.X, cursor.PUNTO.Y);
			}
			if (gh == 4)
			{
				gt.FillRectangle(fr, cursor.PUNTO.X, cursor.PUNTO.Y, 10, 10);

			}

			if (gh == 5)
			{
				gt.FillRectangle(fr, cursor.PUNTO.X, cursor.PUNTO.Y, 10, 10);

			}
			if (gh == 6)
			{
				gt.FillRectangle(fr, cursor.PUNTO.X, cursor.PUNTO.Y, 30, 30);

			}

			if (gh == 7)
			{
				gt.DrawLine(new Pen(fk, 1), cursor.PUNTO.X, cursor.PUNTO.Y, p.X, p.Y);
			}


			if (gh == 8)
			{
				gt.FillEllipse(fr, cursor.PUNTO.X, cursor.PUNTO.Y, 20, 20);
				gt.DrawEllipse(new Pen(Color.Black, 1), cursor.PUNTO.X, cursor.PUNTO.Y, 20, 20);

			}

			if (gh == 9)
			{
				gt.FillEllipse(fr, cursor.PUNTO.X, cursor.PUNTO.Y, 5, 5);
				gt.DrawEllipse(new Pen(Color.Black, 1), cursor.PUNTO.X, cursor.PUNTO.Y, 5, 5);

			}


			if (gh == 10)
			{
				gt.DrawImage(this.pictureBox1.Image, cursor.PUNTO.X, cursor.PUNTO.Y, 200, 61);
			}
          
			return p;
		}
		int alp;
		int alp2=100;
		int gh;
		Color g444;
		private void Form1_Load(object sender, System.EventArgs e)
		{
			
			Cursor.Hide(); 
			g444 = Color.FromArgb(50, h.Next(255), h.Next(255), h.Next(255));
			gh = h.Next(11);
			x = (this.Width / 2) - x;
			y = (this.Height / 2) - y;
			gt = this.CreateGraphics();
			cursor.PUNTO = new PointF(this.Width / 2, this.Height / 2);
		}
		public void forma4(int distancia, int n,Color fk)
		{
			Dibujar_Koch(distancia / 3, n - 1, fk);//a
			cursor.ANGULO = cursor.ANGULO + n / 3;//i
			cursor.ANGULO = float.Parse(Convert.ToString(cursor.ANGULO - ((Math.PI * 2) / 3)));//dd
			cursor.ANGULO = float.Parse(Convert.ToString(cursor.ANGULO - ((Math.PI * 2) / 3)));//dd
			Dibujar_Koch(distancia / 3, n - 1, fk);//a
			cursor.ANGULO = float.Parse(Convert.ToString(cursor.ANGULO - ((Math.PI * 2) / 3)));//dd
			cursor.ANGULO = float.Parse(Convert.ToString(cursor.ANGULO - ((Math.PI * 2) / 3)));//dd
			Dibujar_Koch(distancia / 3, n - 1, fk);//a

			Dibujar_Koch(distancia / 3, n - 1, fk);//a
			cursor.ANGULO = cursor.ANGULO + n / 3;//i
			Dibujar_Koch(distancia / 3, n - 1, fk);//a
			cursor.ANGULO = float.Parse(Convert.ToString(cursor.ANGULO - ((Math.PI * 2) / 3)));//dd
			Dibujar_Koch(distancia / 3, n - 1, fk);//a
			cursor.ANGULO = float.Parse(Convert.ToString(cursor.ANGULO + Math.PI / 3));//i
			Dibujar_Koch(distancia / 3, n - 1, fk);//a

		}
		public void forma3(int distancia, int n,Color fk)
		{
			Dibujar_Koch(distancia / 3, n - 1, fk);//a
			cursor.ANGULO = cursor.ANGULO + n / 3;//i
			Dibujar_Koch(distancia / 3, n - 1, fk);//a
			cursor.ANGULO = float.Parse(Convert.ToString(cursor.ANGULO + Math.PI * 2 / 3));//dd
			Dibujar_Koch(distancia / 3, n - 1, fk);//a
			cursor.ANGULO = float.Parse(Convert.ToString(cursor.ANGULO + Math.PI / 3));//i
			Dibujar_Koch(distancia / 3, n - 1, fk);//a
		}


		public void forma1(int distancia, int n, Color fk)
		{
			Dibujar_Koch(distancia / 3, n - 1, fk);
			cursor.ANGULO = cursor.ANGULO + n / 3;
			Dibujar_Koch(distancia / 3, n - 1, fk);
			cursor.ANGULO = cursor.ANGULO + n * 2 / 3;
			Dibujar_Koch(distancia / 3, n - 1, fk);
			cursor.ANGULO = cursor.ANGULO + n / 3;
			Dibujar_Koch(distancia / 3, n - 1, fk);
		}

		public void forma2(int distancia, int n, Color fk)
		{
			Dibujar_Koch(distancia / 3, n - 1, fk);//a
			cursor.ANGULO = cursor.ANGULO + n / 3;//i
			Dibujar_Koch(distancia / 3, n - 1, fk);//a
			cursor.ANGULO = float.Parse(Convert.ToString(cursor.ANGULO + Math.PI * 2 / 3));//dd
			Dibujar_Koch(distancia / 3, n - 1, fk);//a
			cursor.ANGULO = float.Parse(Convert.ToString(cursor.ANGULO + Math.PI / 3));//i
			Dibujar_Koch(distancia / 3, n - 1, fk);//a


			cursor.ANGULO = float.Parse(Convert.ToString(cursor.ANGULO + Math.PI * 2 / 3));//dd
			cursor.ANGULO = float.Parse(Convert.ToString(cursor.ANGULO + Math.PI * 2 / 3));//dd
			Dibujar_Koch(distancia / 3, n - 1, fk);//a
			cursor.ANGULO = cursor.ANGULO + n / 3;//i
			Dibujar_Koch(distancia / 3, n - 1, fk);//a
			cursor.ANGULO = float.Parse(Convert.ToString(cursor.ANGULO + Math.PI * 2 / 3));//dd
			Dibujar_Koch(distancia / 3, n - 1, fk);//a
			cursor.ANGULO = float.Parse(Convert.ToString(cursor.ANGULO + Math.PI / 3));//i
			Dibujar_Koch(distancia / 3, n - 1, fk);//a
			cursor.ANGULO = cursor.ANGULO + n / 3;//i
			Dibujar_Koch(distancia / 3, n - 1, fk);//a
			cursor.ANGULO = cursor.ANGULO + n / 3;//i
			Dibujar_Koch(distancia / 3, n - 1, fk);//a
			cursor.ANGULO = float.Parse(Convert.ToString(cursor.ANGULO + Math.PI * 2 / 3));//dd
			Dibujar_Koch(distancia / 3, n - 1, fk);//a
			cursor.ANGULO = float.Parse(Convert.ToString(cursor.ANGULO + Math.PI / 3));//i
			Dibujar_Koch(distancia / 3, n - 1, fk);//a

		}
		int ya;
		int f4;
		int ft = 5;


		public void Dibujar_Koch( int distancia ,int n, Color fk)
		{

			if (n ==0)
			{
				cursor.PUNTO = this.avanzar(cursor.ANGULO, distancia,fk);
			}
			else
			{

				if (ft==5)
				{


					if (f4 == 0)
					{

						Dibujar_Koch(distancia / 3, n - 1, fk);//a
						cursor.ANGULO = cursor.ANGULO + n / 3;//i
						Dibujar_Koch(distancia / 3, n - 1, fk);//a
						cursor.ANGULO = float.Parse(Convert.ToString(cursor.ANGULO - ((Math.PI * 2) / 3)));//dd
						Dibujar_Koch(distancia / 3, n - 1, fk);//a
						cursor.ANGULO = float.Parse(Convert.ToString(cursor.ANGULO + Math.PI / 3));//i
						Dibujar_Koch(distancia / 3, n - 1, fk);//a


					}
					if (f4 == 1)
					{

						this.forma1(distancia, n, fk);


					}
					if (f4 == 2)
					{
						this.forma2(distancia, n, fk);
					}
					if (f4 == 3)
					{

						this.forma3(distancia, n, fk);
					}
					if (f4 == 4)
					{

						this.forma4(distancia, n, fk);



					}


				}
				else
				{



					if (ft == 0)
					{

						Dibujar_Koch(distancia / 3, n - 1, fk);//a
						cursor.ANGULO = cursor.ANGULO + n / 3;//i
						Dibujar_Koch(distancia / 3, n - 1, fk);//a
						cursor.ANGULO = float.Parse(Convert.ToString(cursor.ANGULO - ((Math.PI * 2) / 3)));//dd
						Dibujar_Koch(distancia / 3, n - 1, fk);//a
						cursor.ANGULO = float.Parse(Convert.ToString(cursor.ANGULO + Math.PI / 3));//i
						Dibujar_Koch(distancia / 3, n - 1, fk);//a


					}
					if (ft == 1)
					{

						this.forma1(distancia, n, fk);


					}
					if (ft == 2)
					{
						this.forma2(distancia, n, fk);
					}
					if (ft == 3)
					{

						this.forma3(distancia, n, fk);
					}
					if (ft == 4)
					{

						this.forma4(distancia, n, fk);



					}
                
             
				}
               

			}
            
            
		}
		int d;
		int x;
		int y;
		int fdd;
		private void timer1_Tick(object sender, System.EventArgs e)
		{
			if (ya < 100)
			{




				Dibujar_Koch(this.Width, 3, g444);


				if (alp >= 100)
				{
					alp = 0;
				}

				if (alp2 >= 0)
				{
					alp2 = 100;
				}

				alp++;
				alp2--;
			}

			if (ya==25)
			{
				Random fr = new Random();
				g444 = Color.FromArgb(alp, fr.Next(255), fr.Next(255), fr.Next(255));
			}

			if (ya == 50)
			{
				Random fr = new Random();
				g444 = Color.FromArgb(alp, fr.Next(255), fr.Next(255), fr.Next(255));
			}

			if (ya == 75)
			{
				Random fr = new Random();
				g444 = Color.FromArgb(alp, fr.Next(255), fr.Next(255), fr.Next(255));
			}


			if (ya == 100)
			{
				Random fr = new Random();
				g444 = Color.FromArgb(alp, fr.Next(255), fr.Next(255), fr.Next(255));
			}
			if (ya >= 100)
			{
    
				f4 = h.Next(5);
				gh = h.Next(11);
     
				cursor.PUNTO = new PointF(h.Next(this.Width / 2), h.Next(this.Height / 2));
				//ft = f4;

   
				Graphics fd = this.CreateGraphics();
				SolidBrush gt2 = new SolidBrush(Color.FromArgb(d, 0, 0, 0));
				gt.FillEllipse(gt2, (this.Width / 2) - (x / 2), (this.Height / 2) - (y / 2), x, y);

  
				if (fdd >= 100)
				{

					this.Refresh();
					ya = 0;



				}
				x = x + 100;
				y = y + 100;
				fdd=fdd+10;

				if (d < 100)
				{
					d++;
				}

			}
			ya++;

		}

		private void Form1_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Escape)
			{
				this.Close();
			}
		}

		private void Form1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			
		}

		private void Form1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			Application.Exit();
		}



	}
}
